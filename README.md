THIS REPO IS DEPRECATED.

Over past couple of years, much of config management was taken over by [NixOS](https://nixos.org/).

This reduced tracked config of applications down to Doom-Emacs.

It doesn't make sense to hold entire homedir hostage to track single directory, so Doom-Emacs config, going forward,
is tracked under its own repo: https://git.sr.ht/~payas/.doom.d

This repo is only held for historical purpose and eventually will be removed from public internet.
